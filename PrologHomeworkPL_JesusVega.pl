store(best_smoothies, [alan,john,mary],
[smoothie(berry, [orange, blueberry, strawberry], 2),
smoothie(tropical, [orange, banana, mango, guava], 3),
smoothie(blue, [banana, blueberry], 3)]).

store(all_smoothies, [keith,mary],
[ smoothie(pinacolada, [orange, pineapple, coconut], 2),
smoothie(green, [orange, banana, kiwi], 5),
smoothie(purple, [orange, blueberry, strawberry], 2),
smoothie(smooth, [orange, banana, mango],1)]).

store(smoothies_galore, [heath,john,michelle],
[smoothie(combo1, [strawberry, orange, banana], 2),
smoothie(combo2, [banana, orange], 5),
smoothie(combo3, [orange, peach, banana], 2),
smoothie(combo4, [guava, mango, papaya, orange],1),
smoothie(combo5, [grapefruit, banana, pear],1) ]).

/* more_than_four(X) that is true if store X has four or more smoothies on its menu. */
/*Test Query: more_than_four(all_smoothies), should be true */
more_than_four(X) :- store(X,_,L), not(length(L,0)),not(length(L,1)),not(length(L,2)),not(length(L,3)).

/* exists(X) that is true if there is a store that sells a smoothie named X. */
/*Test query: exists(combo4), should be true*/
/*Test query: exists(combo7), should be false*/
exists(X) :- store(_,_,P), member(smoothie(X,_,_),P).

/*ratio(X,R) that is true if there is a store named X, and if R is the ratio of the store's number of employees to the store's number of smoothies on the menu.*/ 
/*test query: ratio(all_smoothies, 2:4)*/
ratio(X,R) :- store(X,E,S), length(E,EM), length(S,SM), R == EM:SM.

/* average(X,A) that is true if there is a store named X, and if A is the average price of the smoothies on the store's menu.*/
/*Test query: average(best_smoothies,8/3)*/
/*Test Query: average(smoothies_galore,11/5)*/
addition([H|T],R) :- H = smoothie(_,_,P), addition(T,A), R is P+A. 
addition([], R) :- R is 0.
average(X,R) :- store(X,_,P), addition(P, SUMAV), length(P,NUM), NUM>0, R == SUMAV/NUM.
